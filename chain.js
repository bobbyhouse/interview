// Write a function that produces new instances of
// a pizza object.

// The object should contain two methods
// "addTopping" and "getToppings."

// The implementation details of internal data
// structures should be private.

// For Example:

// var pizzaOne = pizza();
// pizzaOne.addTopping('anchovies').getToppings();

// var pizzaTwo = pizza();
// pizzaTwo.addTopping('ham').addTopping('pineapple').getToppings();

// >> anchovies
// >> ham, pineapple


///////////////////////////////////////////////////
// SOLUTION BELOW
///////////////////////////////////////////////////

var pizza = (function() {

    var toppings = [];

    return {
        addTopping: function(topping) {
            toppings.push(topping);
            return this;
        },

        getToppings: function() {
            console.log(toppings.join(', '));
            return this;
        },
    };
});




