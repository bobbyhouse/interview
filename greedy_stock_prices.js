// Imagine that you have an array that has the
// prices of a given stock over the course of 6
// hours.

// The index would represent an hour of the day
// and the value at that index would represent
// the price that it was worth at that hour.

// Find the max profit you could get if you
// bought and sold at the right times.

// You cannot sell before you buy.

// var stockPricesYesterday = [10, 7, 5, 8, 11, 9];
// console.log(getMaxProfit(stockPricesYesterday));
//
// >> 6
//
// Max profit would be 6 if you bought it for 5
// and sold it for 11

///////////////////////////////////////////////////
// SOLUTION BELOW
///////////////////////////////////////////////////

var stockPricesYesterday = [10, 7, 5, 8, 11, 9];

var getMaxProfit = (function(stockPricesYesterday) {

	var minPrice = stockPricesYesterday[0];

	var maxProfit = stockPricesYesterday[1] - minPrice;

	for(var i=1; i < stockPricesYesterday.length; i++) {

		var currentPrice = stockPricesYesterday[i];
		var potentialProfit = currentPrice - minPrice;

		maxProfit = Math.max(maxProfit, potentialProfit);
		minPrice = Math.min(minPrice, currentPrice);
	}

	return maxProfit;
});

console.log(getMaxProfit(stockPricesYesterday));
