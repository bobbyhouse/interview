// Two players played a game that has 5 rounds. You
// can assume that each player received a score for
// all 5 rounds and that the scores are non-negative.
// The game is won by the player that wins the most
// rounds.

// Their scores were recorded in an object. The object
// contains two propertie. The key for each property
// is the player's first name and the value is
// their scores for each round, stored as an array.

// Write a function that will print the winner of
// each round. You're function should also print
// the winner of the game.

// For example:
//
// var scores = {
//	alice: [10, 4, 5, 1, 2],
//	bob: [1, 1, 0, 2, 4],
// }
//
// compareScores(scores)
//
// >> Round 1: Alice
// >> Round 2: Alice
// >> Round 3: Alice
// >> Round 4: Bob
// >> Round 5: Bob
// >> Congragulations Alice!


///////////////////////////////////////////////////
// SOLUTION BELOW
///////////////////////////////////////////////////

// They should ask what happens in the event of a
// tie for a round or for the game.

// They should be able to describe the runtime of
// their solution.

// They should be able to describe the space
// requirements of their solution.

// They should not forget to format the output
// correctly.

// They should be able to talk about the different
// branches of logic within their code.

// Something like below would be a sufficent answer

var scores = {
    alice: [0, 0, 0, 0, 0],
    bob: [0, 0, 0, 0, 0],
};

function formatName(name) {
    return name.charAt(0).toUpperCase() + name.slice(1);
}

function printWinner(scores) {
    var players = Object.keys(scores);
    var playerOne = players[0];
    var playerTwo = players[1];

    var playerOneWins = 0;
    var playerTwoWins = 0;

    for(var i=0; i < playerOne.length; i++) {
        if (scores[playerOne][i] > scores[playerTwo][i]) {
            playerOneWins = playerOneWins + 1;
            console.log('Round ' + i + ': ' + formatName(playerOne));

        } else if (scores[playerOne][i] < scores[playerTwo][i]) {
            playerTwoWins = playerTwoWins + 1;
            console.log('Round ' + i + ': ' +  formatName(playerTwo));
        } else {
            console.log('Round ' + i + ': Tie');
        }
    }

    if (playerOneWins > playerTwoWins) {
        console.log('Congragulations ' + formatName(playerOne) + '!');
    } else if (playerOneWins < playerTwoWins) {
        console.log('Congragulations ' + formatName(playerTwo) + '!');
    } else {
        console.log('No Winner');
    }
}

printWinner(scores);
