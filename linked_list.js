function LinkedList () {
    this.head = null;
}

LinkedList.prototype.insert = function(value) {
        var current;

        if (this.head === null) {
            this.head = { value: value, next: null };
            return;
        }

        current = this.head;

        while(current.next !== null) {
            current = current.next;
        }

        current.next = { value: value, next: null };
}

LinkedList.prototype.printAll = function() {
    console.log(this.head);
}

LinkedList.prototype.find = function(value) {
    var current = this.head;

    while(current !== null) {
        if (current.value === value) {
            return current;
        } else {
            current = current.next;
        }
    }

    return null;
}

LinkedList.prototype.delete = function(value) {
    var last;
    var current;

    if (this.head.value === value) {
        this.head = this.head.next;
    }

    last = this.head;
    current = this.head.next;

    while(current !== null) {
        if (current.value === value) {
            last.next = current.next;
            return current;
        }

        last = current;
        current = current.next
    }
}

var ll1 = new LinkedList();

ll1.insert(0);
ll1.insert(2);
ll1.insert(3);
ll1.printAll();
