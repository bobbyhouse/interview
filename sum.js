// Write a function that takes an array as an
// argument and returns the sum of it's elements.

// For example

// var numbers = [1, 2, 3, 4, 5];
// console.log(sum(numbers))
// >> 15


///////////////////////////////////////////////////
// SOLUTION BELOW
///////////////////////////////////////////////////

// They should ask if it is safe to assume that
// items contains only numbers

// You should ask how they would modify to accept
// characters

// You should ask how they would adapt if items may
// contain nulls or undefineds


function sum(items) {
    return items.reduce(function(memo, item) {
        return memo + item;
    }, 0);
}

console.log(sum(numbers));