// implement an event emitter according to the following interface

const emitter = Emitter();

emitter.on('change', function() {
   console.log('change');
});

emitter.emit('change');

// support multiple callbacks per-event name

// support for passing arbitrary number of additional arguments
// emitter.emit('change', 1, {}, 'three');

// support for execution context (ignoring fat-arrow usage)
// emitter.on('change', function() {
//     console.log('change', this);
// }, this);

// support for off all events by name
// emitter.off('change');

// support for off single callback
