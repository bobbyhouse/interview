// Given a sorted array, how quickly could we
// determine if an integer is in the array?

// Write a function that takes an array and a integer
// as arguments and returns either true or false depending
// on whether or not that integer exists in the array.


// For Example:
//
// var array = [7, 8, 9, 10, 20, 33, 60]
//
// console.log(inSortedArray(array, 33));
// console.log(inSortedArray(array, 40));
//
// >> true
// >> false

///////////////////////////////////////////////////
// SOLUTION BELOW
///////////////////////////////////////////////////


// With binary search we can find if the item is in
// a sorted array in O(log(n)) time with O(1)
// additional space.

var array = [7, 8, 9, 10, 20, 33, 60]

var inSortedArray = (function(array, value) {

	var currentIndex;

	var minIndex = 0;
	var maxIndex = array.length - 1;

	while(minIndex <= maxIndex) {

		currentIndex = Math.floor((minIndex + maxIndex) / 2);

		if(value === array[currentIndex]) {
			return true;
		}

		else if(value > array[currentIndex]) {
			minIndex = currentIndex + 1;
		}

		else {
			maxIndex = currentIndex - 1;
		}
	}

	return false;
});

console.log(inSortedArray(array, 33));
console.log(inSortedArray(array, 40));